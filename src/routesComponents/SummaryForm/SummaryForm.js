import React, { useEffect, useState } from 'react';
import { getFormStorage } from '../../helpers/localStorage';
import { Spin, Form } from 'antd';

import {delay} from '../../helpers/functions';
import { NAME_FORM_KEY, NAME_FORM_FIELD } from '../NameForm/NameForm';
import { EMAIL_FORM_KEY, EMAIL_FORM_FIELD } from '../EmailForm/EmailForm';
import { PHONE_FORM_KEY, PHONE_FORM_FIELD } from '../PhoneForm/PhoneForm';
import { SALARY_FORM_KEY, SALARY_FORM_FIELD, SALARY_RANGES } from '../SalaryForm/SalaryForm';


export const SUMMARY_FORM_KEY = 'SUMMARY';

const SummaryForm = (props) => {
    const [values, setValues] = useState({})
    const [isLoading, setIsLoading] = useState(true)
    useEffect( () => {
        const values = getFormStorage() || {};
        delay(1000).then( () => {
            setValues(values);
            setIsLoading(false)
        })
    }, []);

    const renderFormItem = (label, key, cb) => (
        <Form.Item label={label}>
            <span className="ant-form-text"> 
                {values[key] ? cb() : <div className="text-danger">Please go back and fill the {label}</div>}
            </span>
        </Form.Item>
    )
    return (
        <div className="form-step-container home-slide">
            <Spin spinning={isLoading}>
                <Form>
                    {renderFormItem("Name", NAME_FORM_KEY, () => values[NAME_FORM_KEY][NAME_FORM_FIELD])}
                    {renderFormItem("Email", EMAIL_FORM_KEY, () => values[EMAIL_FORM_KEY][EMAIL_FORM_FIELD])}
                    {renderFormItem("Phone", PHONE_FORM_KEY, () => values[PHONE_FORM_KEY][PHONE_FORM_FIELD])}
                    {renderFormItem("Salary", SALARY_FORM_KEY, () => SALARY_RANGES.find( range => range.id === values[SALARY_FORM_KEY][SALARY_FORM_FIELD]).value)}
                </Form>
            </Spin>
        </div>
    )
}


export default SummaryForm;