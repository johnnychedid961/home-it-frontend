/**
 * Helper function that logs the item sent to it
 * @param {any} x 
 */
export const trace = x => {
    console.log(x);
    return x;
}

export const isReactEnvProd = () => process.env.NODE_ENV === 'production';

//for a function to have a mandatory param
export const mandatory = () => { throw new Error('Missing parameter!'); }

//
export const delay = t =>  new Promise(resolve => setTimeout(resolve, t));

export const isSmallScreen = () => window.innerWidth < 650;

//Delay a function from getting called by a certain amount as long as it is getting called
export const debounce = (func, wait, immediate) => {
    let timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};
