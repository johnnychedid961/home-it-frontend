import React from 'react';
import { render, cleanup } from '@testing-library/react';

import App from './App';
import C from "./helpers/constances";

describe('App Auth rendering', () => {
    afterEach(() => {
        cleanup();
    });
    test('renders Login', () => {
        const { getByText }  = render(<App />);
        const linkElement = getByText(/Username/i);
        expect(linkElement).toBeInTheDocument();
    
    });
    test('renders Form', () => {
        Object.defineProperty(window.document, 'cookie', {
            writable: true,
            value: `${C.COOKIE_KEYS.AUTH_TOKEN}=123`,
        });
        const { getByText }  = render(<App />);
        const linkElement = getByText(/Home.it/i);
        expect(linkElement).toBeInTheDocument();
    });
})
