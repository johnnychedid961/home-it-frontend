import React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
import './App.css';

import appRoutesBuilder, { appRoutes } from './routes/appRoutes';
import Login from './routesComponents/Login/Login';
import PrivateRoute from './components/common/PrivateRoute';
import auth from './helpers/auth';
import MainForm from './routesComponents/MainForm/MainForm';

const App = () => {
  const getDefaultRedirect = () => auth.isAuthenticated() ? appRoutesBuilder.getFormUrl() : appRoutesBuilder.getLoginUrl();
  return (
    <Router>
        <Switch>
          <Route path={appRoutes.LOGIN_URL}><Login /></Route>

          <PrivateRoute path={appRoutes.FORM_URL}><MainForm /></PrivateRoute>

          <Redirect to={{ pathname: getDefaultRedirect() }} />
      </Switch>
    </Router>
  )
};

export default App;
