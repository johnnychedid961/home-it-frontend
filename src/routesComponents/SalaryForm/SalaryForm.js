import React, { useEffect } from 'react';
import { Form, Radio  } from 'antd';
import { getFormStorage } from '../../helpers/localStorage';

export const SALARY_RANGES = [
    {
        id: 1,
        value: "0 - 1.000"
    },
    {
        id: 2,
        value: "1.000 - 2.000"
    },
    {
        id: 3,
        value: "2.000 - 3.000"
    },
    {
        id: 4,
        value: "3.000 - 4.000"
    },
    {
        id: 5,
        value: "More than 4.000" // this is in german the exercise question
    },
];


export const SALARY_FORM_KEY = 'SALARY';
export const SALARY_FORM_FIELD = 'salary';

const SalaryForm = React.forwardRef((props, ref) => {
    const [form] = Form.useForm();
    useEffect( () => {
        const values = getFormStorage();
        if(values && values[SALARY_FORM_KEY]) form.setFieldsValue({[SALARY_FORM_FIELD]: values[SALARY_FORM_KEY][SALARY_FORM_FIELD]})
    }, [form]);

    return (
        <div className="form-step-container home-slide">
            <Form layout="inline" form={form} ref={ref}>
                <Form.Item
                    name    ={SALARY_FORM_FIELD}
                    label   ="Salary"
                    rules   ={[
                        {
                            required: true,
                            message: 'Please select a salary Range',
                        },
                    ]}
                >
                    <Radio.Group>
                        {SALARY_RANGES.map( range => <Radio className="d-block" key={range.id} value={range.id}>{range.value}</Radio>)}
                    </Radio.Group>
                </Form.Item>
            </Form>
        </div>
    )
})


export default SalaryForm
