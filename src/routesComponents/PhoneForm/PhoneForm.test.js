import React from 'react';
import { render, cleanup } from '@testing-library/react';

import PhoneForm from './PhoneForm';



describe('PhoneForm Component rendering', () => {
    afterEach(() => {
        cleanup();
    });
    test('renders PhoneForm', () => {
        const { getByText }  = render(<PhoneForm />);
        const linkElement = getByText(/Phone/i);
        expect(linkElement).toBeInTheDocument();
    });
})
