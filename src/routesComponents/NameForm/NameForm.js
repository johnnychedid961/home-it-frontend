import React, { useEffect }  from 'react';
import { Form, Input } from 'antd';
import { getFormStorage } from '../../helpers/localStorage';

export const NAME_FORM_KEY = 'NAME';
export const NAME_FORM_FIELD = 'name';

const NameForm = React.forwardRef((props, ref) => {
    const [form] = Form.useForm();
    useEffect( () => {
        const values = getFormStorage();
        if(values && values[NAME_FORM_KEY]) form.setFieldsValue({[NAME_FORM_FIELD]: values[NAME_FORM_KEY][NAME_FORM_FIELD]})
    }, [form]);

    return (
        <div className="form-step-container home-slide">
            <Form layout="inline" form={form} ref={ref}>
                <Form.Item
                name    ={NAME_FORM_FIELD}
                label   ="Name"
                rules   ={[
                    {
                        required: true,
                        message: 'Username is required!',
                    },
                ]}
                >
                    <Input />
                </Form.Item>
            </Form>
        </div>
    )
})


export default NameForm