//Url used in the router to determine where each route should go
const BASE_URL  = ``;
export const appRoutes = {
    BASE_URL            : `${BASE_URL}`,
    LOGIN_URL           : `${BASE_URL}/login`,
    FORM_URL            : `${BASE_URL}/form`,
    FORM_NAME_URL       : `${BASE_URL}/form/name`,
    FORM_EMAIL_URL      : `${BASE_URL}/form/email`,
    FORM_PHONE_URL      : `${BASE_URL}/form/phone`,
    FORM_SALARY_URL     : `${BASE_URL}/form/salary`,
    FORM_SUMMARY_URL    : `${BASE_URL}/form/summary`,
}

//Used to create specific routes with params to redirect to specific screen
const appRoutesBuilder    = {
    getDefaultLandingPage  : () => appRoutes.FORM_NAME_URL,

    getBaseUrl             : () => appRoutes.BASE_URL,
    getLoginUrl            : () => appRoutes.LOGIN_URL,
    getFormUrl             : () => appRoutes.FORM_URL,
    getFormNameUrl         : () => appRoutes.FORM_NAME_URL,
    getFormEmailUrl        : () => appRoutes.FORM_EMAIL_URL,
    getFormPhoneUrl        : () => appRoutes.FORM_PHONE_URL,
    getFormSalaryUrl       : () => appRoutes.FORM_SALARY_URL,
    getFormSummaryUrl      : () => appRoutes.FORM_SUMMARY_URL,
}

export default appRoutesBuilder ;