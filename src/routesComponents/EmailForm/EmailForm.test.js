import React from 'react';
import { render, cleanup } from '@testing-library/react';

import EmailForm from './EmailForm';



describe('EmailForm Component rendering', () => {
    afterEach(() => {
        cleanup();
    });
    test('renders EmailForm', () => {
        const { getByText }  = render(<EmailForm />);
        const linkElement = getByText(/Email/i);
        expect(linkElement).toBeInTheDocument();
    });
})
