import React from 'react';
import { render, cleanup } from '@testing-library/react';

import NameForm from './NameForm';



describe('NameForm Component rendering', () => {
    afterEach(() => {
        cleanup();
    });
    test('renders NameForm', () => {
        const { getByText }  = render(<NameForm />);
        const linkElement = getByText(/Name/i);
        expect(linkElement).toBeInTheDocument();
    });
})
