import React, { useEffect } from 'react';
import { Form, InputNumber  } from 'antd';
import { getFormStorage } from '../../helpers/localStorage';

export const PHONE_FORM_KEY = 'PHONE';
export const PHONE_FORM_FIELD = 'phone';

const PhoneForm = React.forwardRef((props, ref) => {
    const [form] = Form.useForm();
    useEffect( () => {
        const values = getFormStorage();
        if(values && values[PHONE_FORM_KEY]) form.setFieldsValue({[PHONE_FORM_FIELD]: values[PHONE_FORM_KEY][PHONE_FORM_FIELD]})
    }, [form]);

    return (
        <div className="form-step-container home-slide">
            <Form layout="inline" form={form} ref={ref}>
                <Form.Item
                    name    ={PHONE_FORM_FIELD}
                    label   ="Phone"
                    rules   ={[
                        {
                            required: true,
                            message: 'Phone number is required',
                        },
                    ]}
                >
                    <InputNumber className="w-100"/>
                </Form.Item>
            </Form>
        </div>
    )
})


export default PhoneForm
