import React from 'react';
import { render, cleanup } from '@testing-library/react';

import SalaryForm from './SalaryForm';



describe('SalaryForm Component rendering', () => {
    afterEach(() => {
        cleanup();
    });
    test('renders SalaryForm', () => {
        const { getByText }  = render(<SalaryForm />);
        const linkElement = getByText(/Salary/i);
        expect(linkElement).toBeInTheDocument();
    });
})
