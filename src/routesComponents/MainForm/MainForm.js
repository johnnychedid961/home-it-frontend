import React, { useState, useEffect, useRef } from 'react';
import {Switch,  useHistory, Route, Redirect, matchPath, useLocation } from "react-router-dom";
import { TransitionGroup, CSSTransition } from "react-transition-group";
import { Steps, Layout, Button, Spin } from 'antd';

import C from '../../helpers/constances';
import appRoutesBuilder, { appRoutes } from '../../routes/appRoutes';
import auth from '../../helpers/auth';
import { isSmallScreen as isSmallScreenFn, debounce, trace } from '../../helpers/functions';
import NameForm, { NAME_FORM_KEY } from '../NameForm/NameForm';
import EmailForm, { EMAIL_FORM_KEY } from '../EmailForm/EmailForm';
import PhoneForm, { PHONE_FORM_KEY } from '../PhoneForm/PhoneForm';
import SalaryForm, { SALARY_FORM_KEY } from '../SalaryForm/SalaryForm';
import SummaryForm, { SUMMARY_FORM_KEY } from '../SummaryForm/SummaryForm';
import { setFormStorage, removeFormStorage } from '../../helpers/localStorage';

import './MainForm.css';

const { Header } = Layout;
const { Step } = Steps;

//Can be added into it's own custom Hook File
function usePrevious(value) {
    const ref = useRef();
    useEffect(() => {
      ref.current = value;
    });
    return ref.current;
}
export const STEPS = {
    [NAME_FORM_KEY]: {
        id: 10,
        title: "Name",
        route: appRoutes.FORM_NAME_URL,
        formRef: React.createRef(),
        routeRender: (props, refProp) => <NameForm {...props} {...refProp}/>
    },
    [EMAIL_FORM_KEY]: {
        id: 11,
        title: "Email",
        route: appRoutes.FORM_EMAIL_URL,
        formRef: React.createRef(),
        routeRender: (props, refProp) => <EmailForm {...props} {...refProp}/>

    },
    [PHONE_FORM_KEY]: {
        id: 12,
        title: "Phone",
        route: appRoutes.FORM_PHONE_URL,
          formRef: React.createRef(),
        routeRender: (props, refProp) => <PhoneForm {...props} {...refProp}/>
    },
    [SALARY_FORM_KEY]: {
        id: 13,
        title: "Salary",
        route: appRoutes.FORM_SALARY_URL,
          formRef: React.createRef(),
        routeRender: (props, extraProps) => <SalaryForm {...props} {...extraProps}/>
    },
    [SUMMARY_FORM_KEY]: {
        id: 14,
        title: "Summary",
        route: appRoutes.FORM_SUMMARY_URL,
        formRef: React.createRef(),
        routeRender: props => <SummaryForm {...props} />
    },
}
const STEPS_KEY = Object.keys(STEPS);

const MainForm = () => {
    let history = useHistory();
    let [isLoading, setIsLoading] = useState(false)
    let [currentStep, setCurrentStep] = useState(0)
    let [isPrevNextClass, setIsPrevNextClass] = useState("")
    let [isSmallScreen, setIsSmallScreen] = useState(isSmallScreenFn())
    const prevStateStep = usePrevious(currentStep);
    

    const logout = () => {
        setIsLoading(true);
        auth.signout(() => {
          //No need to setState and remove loader since the comp will be unMounted using the history.replace()
          history.replace(appRoutesBuilder.getLoginUrl());
        });
    };

    const getCurrentStep = () => {
        STEPS_KEY.map( (stepKey, index) => {
            const match = matchPath(window.location.pathname, {
                path: STEPS[stepKey].route,
                exact: true,
                strict: false
            });
            if(match) {
                setCurrentStep(index);
                // Can improve the app by: testing if the user reached this step before by using local storage and take him back to the step (using history) he last made
            }
            return null;
        })
    }
    
    const nextStep = () =>  {
        STEPS[STEPS_KEY[currentStep]].formRef.current.validateFields()
        .then( values => {
            setFormStorage({[STEPS_KEY[currentStep]]: values})
            const nextStep = currentStep + 1;
            setCurrentStep(nextStep);
            setIsPrevNextClass("is-next");
            history.push(STEPS[STEPS_KEY[nextStep]].route);
        } ) 
        .catch( trace )

    }
    const prevStep = () =>  {
        const prevStep = currentStep - 1;
        setCurrentStep(prevStep);
        setIsPrevNextClass("is-prev");
        history.push(STEPS[STEPS_KEY[prevStep]].route);
    }

    //Created a function for this maybe something else is needed when removing all data
    const clearStorage = () => removeFormStorage();

    // Run the effect only on prev --- this can be extracted into it's own hook that runs after a state gets update to mimic setState({}, () => {}) callback function
    useEffect( () => {
        if(prevStateStep && prevStateStep > currentStep) {
            STEPS[STEPS_KEY[currentStep]].formRef.current.validateFields();
        }
    }, [currentStep, prevStateStep]);

    useEffect( () => getCurrentStep())

    useEffect(() => {
        //debounce function and don't allow browser to invoke repeatedly
        const handleResize = debounce( () => {
            setIsSmallScreen(isSmallScreenFn())
        }, 500)
        window.addEventListener('resize', handleResize)
    
        return _ => window.removeEventListener('resize', handleResize)
    })

    return (
        <Spin spinning={isLoading}>
            <Header className="text-right">
                <Button type={C.BUTTON_TYPES.TEXT} className="text-white" onClick={clearStorage} disabled={currentStep === STEPS_KEY.length - 1}>Clear Data</Button> {/* Use this button to test the validations of the form while navigating it*/}
                <Button type={C.BUTTON_TYPES.TEXT} className="text-white" onClick={logout}>Logout</Button>
            </Header>
            <div className="p-20 overflow-hidden">
                <h1>Home.it</h1>
                <Steps progressDot className="mt-50" current={currentStep} direction={isSmallScreen ? "vertical" : "horizontal"}>
                    {STEPS_KEY.map( (step, index) => <Step key={index} title={STEPS[step].title} />)}
                </Steps>

                <div>
                    <div className={`mt-20 ${isPrevNextClass}`} style={{position: "relative"}}>
                        <AnimatedSwitch steps={STEPS} stepsKey={STEPS_KEY}/>
                    </div>

                    <div className="mt-20">
                        <Button type={C.BUTTON_TYPES.PRIMARY} onClick={prevStep}  disabled={currentStep === 0} className="mr-5">Previous</Button>
                        <Button type={C.BUTTON_TYPES.PRIMARY} onClick={nextStep} disabled={currentStep === STEPS_KEY.length - 1 }>Next</Button>
                    </div>
                </div>

            </div>
        </Spin>
    )
};

function AnimatedSwitch({steps, stepsKey}) {
    let location = useLocation();
    return (
        <TransitionGroup>
            <CSSTransition key={location.key} classNames="home-slide" timeout={200}>
                <Switch location={location}>
                    {stepsKey.map( step => <Route key={step} path={steps[step].route} render={props => steps[step].routeRender(props, {ref: steps[step].formRef })} />) }
                    <Redirect to={{ pathname: appRoutesBuilder.getDefaultLandingPage() }} />
                </Switch>
            </CSSTransition>
        </TransitionGroup>
    )
}
export default MainForm