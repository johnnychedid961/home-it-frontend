import React, { useEffect }  from 'react';
import { Form, Input } from 'antd';
import { getFormStorage } from '../../helpers/localStorage';

export const EMAIL_FORM_KEY = 'EMAIL';
export const EMAIL_FORM_FIELD = 'email';


const EmailForm = React.forwardRef((props, ref) => {
    const [form] = Form.useForm();
    useEffect( () => {
        const values = getFormStorage();
        if(values && values[EMAIL_FORM_KEY]) form.setFieldsValue({[EMAIL_FORM_FIELD]: values[EMAIL_FORM_KEY][EMAIL_FORM_FIELD]})
    }, [form]);

    return (
        <div className="form-step-container home-slide">
            <Form layout="inline" form={form} ref={ref}>
                <Form.Item
                name    ={EMAIL_FORM_FIELD}
                label   ="Email"
                rules   ={[
                    {
                        type: "email",
                        required: true,
                        message: 'Email is required!',
                    },
                ]}
                >
                    <Input />
                </Form.Item>
            </Form>
        </div>
    )
})


export default EmailForm