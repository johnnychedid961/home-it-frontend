
const FORM_LOCAL_STORAGE_KEY = "MAIN_FORM";
//helpers create to be able to replace localstorage with any other storage without having to replace it everywhere in the project
export const getFormStorage = () => {
    let value = window.localStorage.getItem(FORM_LOCAL_STORAGE_KEY);
    if(value) value = JSON.parse(value);
    return value;
}
export const setFormStorage = (addToStore) => {
    let formStored = getFormStorage();
    formStored = formStored ? formStored : {};
    window.localStorage.setItem(FORM_LOCAL_STORAGE_KEY, JSON.stringify({...formStored, ...addToStore}));
}
export const removeFormStorage = () => window.localStorage.removeItem(FORM_LOCAL_STORAGE_KEY);
