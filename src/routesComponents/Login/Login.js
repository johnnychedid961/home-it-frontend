import React, { useState } from 'react';
import { Form, Input, Button, Spin } from 'antd';
import { useHistory, useLocation } from 'react-router-dom';

import auth from '../../helpers/auth';
import appRoutesBuilder from '../../routes/appRoutes';

const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};


const Login = () => {

  let history = useHistory();
  let location = useLocation();
  let [isLoading, setIsLoading] = useState(false);

  if(auth.isAuthenticated())  history.replace(appRoutesBuilder.getDefaultLandingPage())

  let { from } = location.state || { from: { pathname: "/" } };
  
  let login = () => {
    setIsLoading(true);
    auth.authenticate(() => {
      // TODO can check where is the last step the user was in and go to that step OR go to the from param

      //No need to setState and remove loader since the comp will be unMounted using the history.replace()
      history.replace(from);
    });
  };


  return (
    <div className="full-screen d-flex-center overflow-hidden">
      <Spin spinning={isLoading}>
        <Form
          {...layout}
          name="basic"
          initialValues={{
            remember: true,
          }}
          onFinish={login}
          >
          <Form.Item
            label="Username"
            name="username"
            rules={[
              {
                required: true,
                message: 'Please input your username!',
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Password"
            name="password"
            rules={[
              {
                required: true,
                message: 'Please input your password!',
              },
            ]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item {...tailLayout}>
            <div className="mb-10">
              Anything goes here just fill the inputs
            </div>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Spin>
    </div>
  );
};


export default Login;