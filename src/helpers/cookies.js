export const setCookie = function(cname, cvalue, cexpires= 365) {
    let d = new Date();
    d.setTime(d.getTime() + (cexpires*24*60*60*1000));
    const expires = 'expires='+d.toUTCString();
    document.cookie = process.env.REACT_APP_ENV === 'production'
                                        ? cname + '=' + cvalue + ';' + expires + ';path=/'
                                        : cname + '=' + cvalue + ';' + expires + ';path=/';
    return true;
};
/**
 * get the cookie by name
 * @param  {string} cname The cookie name
 * @return {string}       The cookie
 */
export const getCookie = function(cname) {
    const name  = cname + '=',
                ca 		= document.cookie.split(';');
    for(let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return '';
};
/**
 * removes the cookie
 * @param  {string} cname the cookie name
 * @return {bool}
 */
export const deleteCookie = function(cname) {
    if(getCookie(cname)) {
        setCookie(cname, null, -1);
    }
    return true;
};