const C = {
    COOKIE_KEYS : {
        AUTH_TOKEN : 'auth_token',
    },
    BUTTON_TYPES : {
        PRIMARY : "primary",
        DEFAULT : "default",
        DASHED  : "dashed",
        TEXT    : "text",
        LINK    : "link",
    }
}
export {C as default};
